﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class WonkyRadio : MonoBehaviour
{
    public AudioMixer master;
    AudioSource audio;
    bool init = false;
    float increment = 1f;
    void Update()
    {
        if (init == false)
        {
            init = true;
            audio = gameObject.GetComponent<AudioSource>();
        }
        if (audio.volume >= 1)
        {
            increment = -.01f;
        }
        
        if (audio.volume <= .5f)
        {
            increment = .01f;
        }

        audio.volume += increment;
        master.SetFloat("My Exposed Param 1", audio.volume);
    }
}
