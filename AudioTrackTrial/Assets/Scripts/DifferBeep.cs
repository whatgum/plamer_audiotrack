﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifferBeep : MonoBehaviour
{
    AudioSource audio;
    bool init = false;
    float increment = 1f;
    void Update()
    {
        if (init == false)
        {
            init = true;
            audio = gameObject.GetComponent<AudioSource>();
        }

        if (audio.pitch >= 1.5)
        {
            increment = -.01f;
        }
        
        if (audio.pitch <= .5f)
        {
            increment = .01f;
        }

        audio.pitch += increment;
        
    }
}
