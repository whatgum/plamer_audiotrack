﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
public class AtmosphereSwitch : MonoBehaviour
{
    private AudioSource audioData;
    public AudioMixerSnapshot spaceship;
    public AudioMixerSnapshot spacewalk;
    public AudioMixer master;

    private void OnTriggerEnter(Collider other)
    {
        audioData = GetComponent<AudioSource>();
        audioData.mute = !audioData.mute;
        GameObject obj = other.gameObject;
        spaceship.TransitionTo(0f);
        master.SetFloat("MyExposedParam", 0f);
    }

    private void OnTriggerExit(Collider other)
    {
        audioData = GetComponent<AudioSource>();
        audioData.mute = !audioData.mute;
        GameObject obj = other.gameObject;
        spacewalk.TransitionTo(5f);
        master.SetFloat("MyExposedParam", -100f);
    }
}
