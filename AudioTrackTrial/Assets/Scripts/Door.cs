﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public float maxDoorMovement = 2.969f;
    public float speed = .05f;
    private bool doorOpen = true;
    private bool doorClosed = true;
    private bool firstTime = false;
   private void OpenDoor()
   {
        if (gameObject.transform.position.z >= maxDoorMovement)
        {
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, maxDoorMovement);
            doorOpen = true;
        }
        else
        {
            gameObject.transform.position += new Vector3(0f, 0f, speed);
        }
   }

    private void CloseDoor()
    {
        if (gameObject.transform.position.z <= 0)
        {
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, -.5f);
            doorClosed = true;
        }
        else
        {
            gameObject.transform.position -= new Vector3(0f, 0f, speed);

        }
    }

    void Update()
    {
        if (doorOpen == false)
        {   
            if (firstTime == true)
            {
                AudioSource audioData = GetComponent<AudioSource>();
                audioData.Play(0);
                firstTime = false;
            }

            OpenDoor();
        }
        else if (doorClosed == false)
        {
            if (firstTime == true)
            {
                AudioSource audioData = GetComponent<AudioSource>();
                audioData.Play(0);
                firstTime = false;
            }

            CloseDoor();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        firstTime = true;
        doorOpen = false;
    }

    private void OnTriggerExit(Collider other)
    {
        firstTime = true;
        doorClosed = false;
    }

}

